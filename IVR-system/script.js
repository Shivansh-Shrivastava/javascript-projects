
let msg1        = "[1] for your data info.      \n";
let msg2        = "[2] for your balance info.   \n";
let msg3        = "[3] for recharge info.       \n";
let msg4        = "[4] to exit.";

let msg             = msg1 + msg2 + msg3 + msg4 + "\n ";
let msgErr          = "Unknown value.";

let dataInfo        = "500MB Left.";
let balanceInfo     = "18 days left for your 1.5GB per-day pack.";
let rechargeInfo    = "199rs pack for 1.5GB data/per-day for 31 days."


let exit = 0;


while (exit === 0)
{
    let userChoice = prompt(msg);

    switch (userChoice)
    {
        case '1':   alert(dataInfo);
                    break;

        case '2':   alert(balanceInfo);
                    break;

        case '3':   alert(rechargeInfo);
                    break;

        case '4':   exit = 1;
                    break;

        default:    alert(msgErr);
                    break;
    }
}